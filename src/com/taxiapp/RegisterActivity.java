package com.taxiapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterActivity extends Activity {

	View v;
	Intent register = new Intent();

	EditText edt_name, edt_phone, edt_pass, edt_email;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		edt_name = (EditText) findViewById(R.id.edt_name);
		edt_phone = (EditText) findViewById(R.id.edt_phone);
		edt_pass = (EditText) findViewById(R.id.edt_password);
		edt_email = (EditText) findViewById(R.id.edt_email);

	}

	public void regClick(View v) {

		switch (v.getId()) {
		case R.id.btn_reg:

			if (edt_name.getText().length() >= 6
					&& edt_name.getText().length() <= 10) {
				if (edt_phone.getText().length() >= 6
						&& edt_phone.length() <= 10) {
					if (edt_pass.getText().length() >= 10
							&& edt_pass.length() <= 15) {
						register.setClass(RegisterActivity.this,
								ForgetPasswordActivity.class);
						startActivity(register);
						finish();
					} else {
						Toast.makeText(RegisterActivity.this,
								"Enter valid Password", 1000).show();
					}
				} else {
					Toast.makeText(RegisterActivity.this,
							"Enter Valid Phone number", 1000).show();
				}
			} else {
				Toast.makeText(RegisterActivity.this, "Enter Valid Username",
						1000).show();
			}
			break;

		default:
			break;
		}

		overridePendingTransition(R.anim.right_in, R.anim.left_out);

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.animation_enter,
				R.anim.animation_leave);

	}

}
