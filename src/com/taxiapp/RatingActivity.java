package com.taxiapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class RatingActivity extends Activity {

	RatingBar rating;
	Button btn_submit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rating_layout);
		rating=(RatingBar)findViewById(R.id.ratingBar);
		
		btn_submit=(Button)findViewById(R.id.btn_submit_rating);
		btn_submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent submit=new Intent(RatingActivity.this,MenuViewActivity.class);
				startActivity(submit);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
		
	}
}
