package com.taxiapp;

import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.taxiapp.fragment.MenuDetailFragment;
import com.taxiapp.fragment.ProfileFragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MenuViewActivity extends Activity {
	String[] menu;
	DrawerLayout dLayout;
	ListView dList;
	ArrayAdapter<String> adapter;
	private GoogleMap googleMap;

	ImageView menu_btn;
	ListView drawer_lst;

	TextView txt_address;

	double lat, longt;
	LatLng methanagar = new LatLng(13.067867500000000000, 80.227049599999980000);
	LatLng natural = new LatLng(13.082680, 80.270718);
	LatLng ram = new LatLng(13.082680, 80.270718);
	LatLng pyrofo=new LatLng(13.037377, 80.212282);
	
	ImageView imgvwMenu, img_searching;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private ListView mDrawerList;
	private String[] mSlideMenuList;

	public boolean killMe = false;
	// For this status
	TextView txt_status, txt_later, txt_now;
	TextView txt_time, txt_EndTime;
	int count = 0;
	Polyline line;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);

		txt_address = (TextView) findViewById(R.id.adds);
		img_searching = (ImageView) findViewById(R.id.img_search);

		txt_now = (TextView) findViewById(R.id.txt_now);
		txt_later = (TextView) findViewById(R.id.txt_later);
		txt_status = (TextView) findViewById(R.id.txt_status);
		txt_time = (TextView) findViewById(R.id.txt_time);
		txt_EndTime = (TextView) findViewById(R.id.txt_times);
		txt_now.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				line = googleMap.addPolyline((new PolylineOptions())
						.add(new LatLng(lat, longt), methanagar).width(20)
						.color(Color.RED).geodesic(true));

				txt_status.setVisibility(View.VISIBLE);
				txt_now.setVisibility(View.GONE);
				txt_later.setVisibility(View.GONE);
				txt_time.setVisibility(View.VISIBLE);
				txt_EndTime.setVisibility(View.VISIBLE);
				googleMap.addCircle(new CircleOptions().center(methanagar)
						.radius(10).strokeColor(Color.BLACK)
						.fillColor(Color.YELLOW));

			}
		});

		txt_status.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				googleMap.clear();
				googleMap
				.addMarker(new MarkerOptions()
						.position(natural)
						.title("Melbourne")
						.snippet(
								"Population: 4,137,400")
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.car_taxis)));
				line = googleMap.addPolyline((new PolylineOptions())
						.add(new LatLng(lat, longt), natural).width(20)
						.color(Color.RED).geodesic(true));
				txt_time.setText(" 0 MIN");
				txt_EndTime.setText(" 0 MIN");
				line.remove();
				/*Toast.makeText(MenuViewActivity.this, "enter on satus", 1000)
						.show();*/
				statuschanges();
			}
		});

		img_searching.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				initilizeMap();
				findlocation();
			}
		});

		imgvwMenu = (ImageView) findViewById(R.id.imgvw_menu);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.slidermenu_main);
		mSlideMenuList = getResources().getStringArray(R.array.menu_array);

		imgvwMenu.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
					mDrawerLayout.openDrawer(Gravity.LEFT);
					mDrawerLayout.bringToFront();
				} else {
					mDrawerLayout.closeDrawer(Gravity.LEFT);
				}

			}
		});

		mDrawerList = (ListView) findViewById(R.id.left_drawer);
		mDrawerList.setAdapter(new ArrayAdapter<String>(MenuViewActivity.this,
				R.layout.drawer_list_item, mSlideMenuList));
		mDrawerList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						if (position == 0) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);
							/*
							 * Intent first = new Intent(MenuViewActivity.this,
							 * MenuViewActivity.class); startActivity(first);
							 */
							overridePendingTransition(R.anim.right_in,
									R.anim.left_out);
						}
						if (position == 1) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);

							Intent first = new Intent(MenuViewActivity.this,
									ProfileActivity.class);
							startActivity(first);

							overridePendingTransition(R.anim.right_in,
									R.anim.left_out);

						}
						if (position == 2) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);
							Intent rating = new Intent(MenuViewActivity.this,
									RatingActivity.class);
							startActivity(rating);
							/*
							 * FragmentManager fragmentManager =
							 * getFragmentManager(); FragmentTransaction
							 * fragmentTransaction = fragmentManager
							 * .beginTransaction(); ProfileFragment fragment =
							 * new ProfileFragment();
							 * fragmentTransaction.replace(R.id.frame_container,
							 * fragment); fragmentTransaction.commit();
							 */
							overridePendingTransition(R.anim.right_in,
									R.anim.left_out);

						}
						if (position == 3) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);
							Intent payment = new Intent(MenuViewActivity.this,
									PaymentActivity.class);
							startActivity(payment);
							overridePendingTransition(R.anim.right_in,
									R.anim.left_out);

						}
						if (position == 4) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);
							Intent reporting = new Intent(
									MenuViewActivity.this,
									ReportIssuesActivity.class);
							startActivity(reporting);
							overridePendingTransition(R.anim.right_in,
									R.anim.left_out);

						}
						if (position == 5) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);

							Intent phoneIntent = new Intent(Intent.ACTION_CALL);
							phoneIntent.setData(Uri
									.parse("tel:91-000-000-0000"));
							try {
								startActivity(phoneIntent);
							}

							catch (android.content.ActivityNotFoundException ex) {
								Toast.makeText(getApplicationContext(),
										"yourActivity is not founded",
										Toast.LENGTH_SHORT).show();
							}

						}
						if (position == 6) {
							mDrawerLayout.closeDrawer(Gravity.LEFT);
							Intent loginpage = new Intent(
									MenuViewActivity.this, LoginActivity.class);
							loginpage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
									| Intent.FLAG_ACTIVITY_CLEAR_TASK);
							startActivity(loginpage);
							overridePendingTransition(R.anim.right_in,
									R.anim.left_out);

						}

					}
				});

		try {
			// Loading map

			initilizeMap();
			findlocation();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	
	private String getDirectionsUrl(double lat, double lng, double lat1,
			double lng1) {
		String str_origin = "origin=" + lat + "," + lng;
		String str_dest = "destination=" + lat1 + "," + lng1;
		String sensor = "sensor=false";
		String parameters = str_origin + "&" + str_dest + "&" + sensor;
		String output = "json";
		String url = "https://maps.googleapis.com/maps/api/directions/"
				+ output + "?" + parameters;
		return url;
		
	}

	void statuschanges() {
		final long start = SystemClock.uptimeMillis();
		final long duration = 500;
		
		Thread t = new Thread() {
			

			@Override
			public void run() {
				try {
					while (killMe==false) {
						Thread.sleep(5000);
						count = count + 1;
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if (count == 1) {
									
									txt_time.setText(" 0 MIN");
									txt_EndTime.setText(" 2 MIN");
									googleMap.clear();
									updatingMarker();
									googleMap.addMarker(new MarkerOptions()
													.position(
															new LatLng(lat,
																	longt))
													.title("Melbourne")
													.snippet(
															"Population: 4,137,400")
													.icon(BitmapDescriptorFactory
															.fromResource(R.drawable.car_taxis)));
									CameraPosition cameraPosition = new CameraPosition.Builder()
									.target(new LatLng(lat, longt)) // Sets thet center of the
									.zoom(20) // Sets the zoom
									.bearing(90) // Sets the orientation of the camera to east
									.tilt(40) // Sets the tilt of the camera to 30 degrees
									.build(); // Creates a CameraPosition from the builder
							googleMap.animateCamera(CameraUpdateFactory
									.newCameraPosition(cameraPosition));

									
									
									txt_status.setText("Driver Arrived");

								} else if (count == 2) {
									googleMap.clear();
									updatingMarker();
									
									googleMap
									.addMarker(new MarkerOptions()
											.position(new LatLng(lat,longt))
											.title("Melbourne")
											.snippet(
													"Population: 4,137,400")
											.icon(BitmapDescriptorFactory
													.fromResource(R.drawable.car_taxis)));
									
									txt_time.setText(" 2 MIN");
									txt_EndTime.setText(" 0 MIN");
									txt_status.setText("Trip Started");
								} else if (count == 3) {
									googleMap.clear();
									updatingMarker();
									getDirectionsUrl(lat,longt,13.082680, 80.270718);
									googleMap
									.addMarker(new MarkerOptions()
											.position(ram)
											.title("Melbourne")
											.snippet(
													"Population: 4,137,400")
											.icon(BitmapDescriptorFactory
													.fromResource(R.drawable.car_taxis)));
									CameraPosition cameraPosition = new CameraPosition.Builder()
									.target(ram) // Sets thet center of the
									.zoom(20) // Sets the zoom
									.bearing(90) // Sets the orientation of the camera to east
									.tilt(40) // Sets the tilt of the camera to 30 degrees
									.build(); // Creates a CameraPosition from the builder
							googleMap.animateCamera(CameraUpdateFactory
									.newCameraPosition(cameraPosition));

									line = googleMap.addPolyline((new PolylineOptions())
											.add(new LatLng(lat, longt), ram).width(20)
											.color(Color.RED).geodesic(true));
									txt_time.setText(" 2 MIN");
									txt_EndTime.setText(" 4 MIN");
									txt_status.setText("On The Way");
								} else if (count == 4) {
									killMe = true;
									googleMap.clear();
									updatingMarker();
									googleMap
									.addMarker(new MarkerOptions()
											.position(pyrofo)
											.title("Melbourne")
											.snippet(
													"Population: 4,137,400")
											.icon(BitmapDescriptorFactory
													.fromResource(R.drawable.car_taxis)));
									CameraPosition cameraPosition = new CameraPosition.Builder()
									.target(pyrofo) // Sets thet center of the
									.zoom(20) // Sets the zoom
									.bearing(90) // Sets the orientation of the camera to east
									.tilt(40) // Sets the tilt of the camera to 30 degrees
									.build(); // Creates a CameraPosition from the builder
							googleMap.animateCamera(CameraUpdateFactory
									.newCameraPosition(cameraPosition));

									line = googleMap.addPolyline((new PolylineOptions())
											.add(new LatLng(lat, longt), ram, pyrofo).width(20)
											.color(Color.RED).geodesic(true));
									txt_time.setText(" 2 MIN");
									txt_EndTime.setText(" 10 MIN");
									txt_status.setText("Trip Completed");
									
								} else {
									Intent reporting = new Intent(
											MenuViewActivity.this,
											RatingActivity.class);
									startActivity(reporting);
									finish();
									killMe = true;
									overridePendingTransition(R.anim.right_in,
											R.anim.left_out);

								}

							}
						});
					}
				} catch (InterruptedException e) {
				}
			}
		};

		t.start();

	}

	private void initilizeMap() {
		// TODO Auto-generated method stub
		if (googleMap == null) {
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map1)).getMap();
			findlocation();
			// latitude and longitude
			double latitude = lat;
			double longitude = longt;

			// create marker
			MarkerOptions marker = new MarkerOptions().position(
					new LatLng(latitude, longitude)).title("Current Location");
			// Changing marker icon
			marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.rman));

			// adding marker
			googleMap.addMarker(marker);

			Toast.makeText(MenuViewActivity.this,
					"latitandlong" + latitude + "long" + longitude, 1000)
					.show();

			CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(new LatLng(lat, longt)) // Sets thet center of the
					.zoom(20) // Sets the zoom
					.bearing(90) // Sets the orientation of the camera to east
					.tilt(40) // Sets the tilt of the camera to 30 degrees
					.build(); // Creates a CameraPosition from the builder
			googleMap.animateCamera(CameraUpdateFactory
					.newCameraPosition(cameraPosition));

			LatLng MELBOURNE = new LatLng(13.0677, 80.26555);
			LatLng Loyola_college = new LatLng(13.06309800000000,
					80.234400499999990000);
			LatLng nelson = new LatLng(13.068890400000000000,
					80.225656299999970000);
			LatLng methanagar = new LatLng(13.067867500000000000,
					80.227049599999980000);
			LatLng methanagar1 = new LatLng(13.067867600000000000,
					80.227049580000);
			LatLng cholaimedu = new LatLng(13.0604337, 80.2237660);
			LatLng skywalk = new LatLng(13.073700000000000000,
					80.220972999999960000);
			LatLng nungampakkam = new LatLng(13.0640141, 80.23252339);

			googleMap.addMarker(new MarkerOptions()
					.position(MELBOURNE)
					.title("Melbourne")
					.snippet("Population: 4,137,400")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.car_taxis)));

			googleMap.addMarker(new MarkerOptions()
					.position(Loyola_college)
					.title("Melbourne")
					.snippet("Population: 4,137,400")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.car_taxis)));

			googleMap.addMarker(new MarkerOptions()
					.position(nelson)
					.title("Melbourne")
					.snippet("Population: 4,137,400")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.car_taxis)));

			googleMap.addMarker(new MarkerOptions()
					.position(methanagar)
					.title("Melbourne")
					.snippet("Population: 4,137,400")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.car_taxis)));

			googleMap.addMarker(new MarkerOptions()
					.position(cholaimedu)
					.title("Melbourne")
					.snippet("Population: 4,137,400")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.car_taxis)));

			googleMap.addMarker(new MarkerOptions()
					.position(skywalk)
					.title("Melbourne")
					.snippet("Population: 4,137,400")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.car_taxis)));

			googleMap.addMarker(new MarkerOptions()
					.position(methanagar1)
					.title("Melbourne")
					.snippet("Population: 4,137,400")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.car_taxis)));

			googleMap.addMarker(new MarkerOptions()
					.position(nungampakkam)
					.title("Melbourne")
					.snippet("Population: 4,137,400")
					.icon(BitmapDescriptorFactory
							.fromResource(R.drawable.car_taxis)));

			// check if map is created successfully or not
			if (googleMap == null) {
				Toast.makeText(getApplicationContext(),
						"Sorry! unable to create maps", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	private LatLng LatLng(double d, double e) {
		// TODO Auto-generated method stub
		return null;
	}

	public void findlocation() {
		GPSTracker gpsTracker = new GPSTracker(this);
		if (gpsTracker.getIsGPSTrackingEnabled()) {
			String stringLatitude = String.valueOf(gpsTracker.latitude);
			lat = gpsTracker.latitude;
			String stringLongitude = String.valueOf(gpsTracker.longitude);
			longt = gpsTracker.longitude;
			String country = gpsTracker.getCountryName(this);
			String city = gpsTracker.getLocality(this);
			String postalCode = gpsTracker.getPostalCode(this);
			String addressLine = gpsTracker.getAddressLine(this);
			txt_address.setText(addressLine + "," + city + "," + country + ","
					+ postalCode);

		} else {

			gpsTracker.showSettingsAlert();
		}

	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.imgvw_menu:
			if (!mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
				mDrawerLayout.openDrawer(Gravity.LEFT);
				mDrawerLayout.bringToFront();
			} else {
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			}
			break;
		}
	}

	public void updatingMarker() {

		

		LatLng MELBOURNE = new LatLng(13.0677, 80.26555);
		LatLng Loyola_college = new LatLng(13.06309800000000,
				80.234400499999990000);
		LatLng nelson = new LatLng(13.068890400000000000, 80.225656299999970000);
		LatLng methanagar1 = new LatLng(13.067867600000000000, 80.227049580000);
		LatLng cholaimedu = new LatLng(13.0604337, 80.2237660);
		LatLng skywalk = new LatLng(13.073700000000000000,
				80.220972999999960000);
		LatLng nungampakkam = new LatLng(13.0640141, 80.23252339);

		googleMap.addMarker(new MarkerOptions()
				.position(MELBOURNE)
				.title("Melbourne")
				.snippet("Population: 4,137,400")
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.car_taxis)));

		googleMap.addMarker(new MarkerOptions()
				.position(Loyola_college)
				.title("Melbourne")
				.snippet("Population: 4,137,400")
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.car_taxis)));

		googleMap.addMarker(new MarkerOptions()
				.position(nelson)
				.title("Melbourne")
				.snippet("Population: 4,137,400")
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.car_taxis)));

		

		googleMap.addMarker(new MarkerOptions()
				.position(cholaimedu)
				.title("Melbourne")
				.snippet("Population: 4,137,400")
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.car_taxis)));

		googleMap.addMarker(new MarkerOptions()
				.position(skywalk)
				.title("Melbourne")
				.snippet("Population: 4,137,400")
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.car_taxis)));

		googleMap.addMarker(new MarkerOptions()
				.position(methanagar1)
				.title("Melbourne")
				.snippet("Population: 4,137,400")
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.car_taxis)));

		googleMap.addMarker(new MarkerOptions()
				.position(nungampakkam)
				.title("Melbourne")
				.snippet("Population: 4,137,400")
				.icon(BitmapDescriptorFactory
						.fromResource(R.drawable.car_taxis)));

	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.animation_enter,
				R.anim.animation_leave);

	}

}
