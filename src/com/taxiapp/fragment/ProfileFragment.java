package com.taxiapp.fragment;



import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import com.taxiapp.R;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.MediaColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ProfileFragment extends Fragment{
	
	
	protected static final int SELECT_FILE = 1;
	protected static final int REQUEST_CAMERA = 1;
	private static final int RESULT_OK = 1;
	ImageView img_selectimage;
	private static int RESULT_LOAD_IMAGE = 1;
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.profile, container, false);
     
        img_selectimage=(ImageView)view.findViewById(R.id.img_selectimage);
        
        img_selectimage.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
				
				selectImage();
             
			}
		});
        
        return view;
    }

	
		

	private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };
 
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Choose from Library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	super.onActivityResult(requestCode, resultCode, data);
	if (resultCode == RESULT_OK) {
	if (requestCode == REQUEST_CAMERA) {
	Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
	        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
	        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
	 
	        File destination = new File(Environment.getExternalStorageDirectory(),
	                System.currentTimeMillis() + ".jpg");
	 
	        FileOutputStream fo;
	        try {
	            destination.createNewFile();
	            fo = new FileOutputStream(destination);
	            fo.write(bytes.toByteArray());
	            fo.close();
	        } catch (FileNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	 
	        img_selectimage.setImageBitmap(thumbnail);
	       
	 
	} else if (requestCode == SELECT_FILE) {
	Uri selectedImageUri = data.getData();
	        String[] projection = { MediaColumns.DATA };
	        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
	                null);
	        int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
	        cursor.moveToFirst();
	 
	        String selectedImagePath = cursor.getString(column_index);
	 
	        Bitmap bm;
	        BitmapFactory.Options options = new BitmapFactory.Options();
	        options.inJustDecodeBounds = true;
	        BitmapFactory.decodeFile(selectedImagePath, options);
	        final int REQUIRED_SIZE = 200;
	        int scale = 1;
	        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
	                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
	            scale *= 2;
	        options.inSampleSize = scale;
	        options.inJustDecodeBounds = false;
	        bm = BitmapFactory.decodeFile(selectedImagePath, options);
	 
	        img_selectimage.setImageBitmap(bm);
	}
	}
	}
	



	private Cursor managedQuery(Uri selectedImageUri, String[] projection,
			Object object, Object object2, Object object3) {
		// TODO Auto-generated method stub
		return null;
	}
}