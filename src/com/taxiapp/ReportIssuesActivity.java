package com.taxiapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class ReportIssuesActivity extends Activity {

	
	TextView txt_submit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sub_blue);
		txt_submit=(TextView)findViewById(R.id.txt_submit);
		
		
		txt_submit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent submit=new Intent(ReportIssuesActivity.this,MenuViewActivity.class);
				startActivity(submit);
				finish();
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
			}
		});
		
	}
}
